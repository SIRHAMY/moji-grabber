package main

import (
	"fmt"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"os"
)

func main() {
	fmt.Printf("Initializing...\n")

	fmt.Printf("Attempting open of config.json...\n")
	jsonFile, err := os.Open("config.json")
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()

	fmt.Printf("Reading config.json into memory...\n")
	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		fmt.Println(err)
	}

	// Initialize comics so we can read into it
	var comics Comics

	fmt.Printf("Unmarshaling json...\n")
	json.Unmarshal(byteValue, &comics)

	// fmt.Printf("mojiId: ")
	// fmt.Println(comics.MojiId)
	// fmt.Printf("\n")

	var directoryPath string = "./downloads/" + comics.MojiId

	// Check if directory exists
	var _, statError = os.Stat(directoryPath)
	if statError == nil {
		fmt.Printf("There already exists a directory at %#v, exiting...", directoryPath)
		return
	}

	fmt.Printf("Creating directory %#v...", comics.MojiId)
	os.Mkdir(directoryPath, os.ModeDir)

	// Iterate over keys in dict
	// For key in dict, iterate over each file id
	for k, v := range comics.Comics {
		fmt.Printf("key found: %#v \n", k)
		for _, comicId := range v {
			// fmt.Printf("%#v", createBitMojiUrl(comics.MojiId, comic))
			var downloadUrl = createBitMojiUrl(comics.MojiId, comicId)

			writePath := directoryPath + "/" + comicId
			err := downloadFile(downloadUrl, writePath)
			if err != nil {
				fmt.Println(err)
			}
		}
	}

	// Read in dict from config
	// create subfolder for this id
	// Parse over dict keys and download each from url, save with id name
	// output dict that js can read with filepaths
	// das it
}

func createBitMojiUrl(avatarId string, comicId string) string {
	return "https://render.bitstrips.com/v2/cpanel/" + comicId + "-" + avatarId + "-v1.png?transparent=1&palette=1"
}

// Referenced from: https://golangcode.com/download-a-file-from-a-url/
func downloadFile(downloadUrl string, writePath string) error {
	fmt.Printf("Attempting to create file at %#v", writePath)
	out, err := os.Create(writePath)
	if err != nil {
		return err
	}
	defer out.Close()

	response, err := http.Get(downloadUrl)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	_, err = io.Copy(out, response.Body)
	if err != nil {
		return err
	}

	return nil
}

type Comics struct {
	MojiId string `json:"mojiId"`
	Comics map[string][]string `json:"comics"`
}

// https://render.bitstrips.com/render/comicId/avatarId-v3.png?transparent=1&scale=1
// https://render.bitstrips.com/v2/cpanel/comicId/avatarId-v3.png?transparent=1&scale=1
// MyId: cff80ec8-1754-4b9a-9238-3c3c4ae049be
// First: https://render.bitstrips.com/v2/cpanel/3241e7c5-b65a-49f9-b21c-06141f09c08c-cff80ec8-1754-4b9a-9238-3c3c4ae049be-v1.png?transparent=1&palette=1
// Second https://render.bitstrips.com/v2/cpanel/0424dcbb-39cf-4a88-8703-1e58e0d3a306-cff80ec8-1754-4b9a-9238-3c3c4ae049be-v1.png?transparent=1&palette=1
// Stuffl https://render.bitstrips.com/v2/cpanel/6e515ba6-ad8b-4a7e-ba4f-4e7f41fc233e-cff80ec8-1754-4b9a-9238-3c3c4ae049be-v1.png?transparent=1&palette=1

// kebabMe: https://render.bitstrips.com/v2/cpanel/3241e7c5-b65a-49f9-b21c-06141f09c08c-cff80ec8-1754-4b9a-9238-3c3c4ae049be-v1.png?transparent=1&palette=1
// kebabDan https://render.bitstrips.com/v2/cpanel/3241e7c5-b65a-49f9-b21c-06141f09c08c-f5fbfd47-f0c9-4257-b317-d7cd37c81010-v1.png?transparent=1&palette=1
// kebabBob